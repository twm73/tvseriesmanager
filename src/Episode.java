import java.time.LocalDate;
import java.util.ArrayList;

public class Episode {
	private String title;
	private int episodeNumber;
	private int lenght;
	private LocalDate premiere;
	ArrayList<Actor> Actors = new ArrayList<Actor>();
	ArrayList<Director> Directors = new ArrayList<Director>();

	public Episode(String title, int episodeNumber, int lenght, LocalDate premiere) {
		this.title = title;
		this.episodeNumber = episodeNumber;
		this.lenght = lenght;
		this.premiere = premiere;
	}

	public ArrayList<Actor> getActors() {
		return Actors;
	}

	public void setActors(ArrayList<Actor> actors) {
		Actors = actors;
	}

	public ArrayList<Director> getDirectors() {
		return Directors;
	}

	public void setDirectors(ArrayList<Director> directors) {
		Directors = directors;
	}

	public String getTitle() {
		return title;
	}

	public int getEpisodeNumber() {
		return episodeNumber;
	}

	public int getLenght() {
		return lenght;
	}

	public LocalDate getPremiere() {
		return premiere;
	}

}
