import java.util.ArrayList;

public class Season {
	private int seasonNumber;
	private int firstRelaseYear;
	ArrayList<Episode> Episods = new ArrayList<Episode>();
	public Season (int seasonNumber, int firstRelaseYear){
		this.seasonNumber = seasonNumber;
		this.firstRelaseYear = firstRelaseYear;
	}
	public ArrayList<Episode> getEpisods() {
		return Episods;
	}
	public void setEpisods(ArrayList<Episode> episods) {
		Episods = episods;
	}
	public int getSeasonNumber() {
		return seasonNumber;
	}
	public int getFirstRelaseYear() {
		return firstRelaseYear;
	}
	
}
