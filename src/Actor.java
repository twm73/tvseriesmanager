import java.time.LocalDate;

public class Actor {
	private String firstName;
	private String lastName;
	private String biography;
	private LocalDate dateOfBirth;

	public Actor(String firstName, String lastName, String biography, LocalDate dateOfBirth) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.biography = biography;
		this.dateOfBirth = dateOfBirth;
	}

	public String getBiography() {
		return biography;
	}

	public void setBiography(String biography) {
		this.biography = biography;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLastName() {
		return lastName;
	}

	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}

}
