import java.time.LocalDate;

public class Director {
	private String firstName;
	private String lastName;
	private String biography;
	private LocalDate dateOfBirth;
	
	public Director(String firstName, String lastName, String biography, LocalDate dateOfBirth) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.biography = biography;
		this.dateOfBirth = dateOfBirth;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getBiography() {
		return biography;
	}

	public void setBiography(String biography) {
		this.biography = biography;
	}

	public String getFirstName() {
		return firstName;
	}

	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}
	
	
}
